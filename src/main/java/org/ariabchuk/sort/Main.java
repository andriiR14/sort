package org.ariabchuk.sort;

import java.util.*;

public class Main {
    public static void main(String [] args) {
        Sort sort = new Sort();
        Random rand = new Random();
        List<Integer> unsortedList1 = new ArrayList<Integer>();
        int i = 0;
        while (i++ < 1000) {
            unsortedList1.add(rand.nextInt(10000));
        }
        List<Integer> unsortedList2 = new ArrayList<>(unsortedList1);
        List<Integer> unsortedList3 = new LinkedList<>(unsortedList1);
        List<Integer> unsortedList4 = new LinkedList<>(unsortedList1);


        System.out.println("Bubble sorting ArrayList");
        System.out.println(Arrays.toString(unsortedList1.toArray()));
        long startTime = new Date().getTime();
        sort.byBubble(unsortedList1);
        long endTime = new Date().getTime();
        System.out.println("List was sorted for " + (endTime - startTime) + " milliseconds");
        System.out.println(Arrays.toString(unsortedList1.toArray()));

        System.out.println("Gnome sorting ArrayList");
        System.out.println(Arrays.toString(unsortedList2.toArray()));
        startTime = new Date().getTime();
        sort.byGnome(unsortedList2);
        endTime = new Date().getTime();
        System.out.println("List was sorted for " + (endTime - startTime) + " milliseconds");
        System.out.println(Arrays.toString(unsortedList2.toArray()));

        System.out.println("Bubble sorting LinkedList");
        System.out.println(Arrays.toString(unsortedList3.toArray()));
        startTime = new Date().getTime();
        sort.byBubble(unsortedList3);
        endTime = new Date().getTime();
        System.out.println("List was sorted for " + (endTime - startTime) + " milliseconds");
        System.out.println(Arrays.toString(unsortedList3.toArray()));

        System.out.println("Gnome sorting LinkedList");
        System.out.println(Arrays.toString(unsortedList4.toArray()));
        startTime = new Date().getTime();
        sort.byGnome(unsortedList4);
        endTime = new Date().getTime();
        System.out.println("List was sorted for " + (endTime - startTime) + " milliseconds");
        System.out.println(Arrays.toString(unsortedList4.toArray()));
    }
}
