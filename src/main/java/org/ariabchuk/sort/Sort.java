package org.ariabchuk.sort;

import java.util.ArrayList;
import java.util.List;

public class Sort {

    public void byBubble(List<Integer> list) {
        int temp;
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i -1; j++) {
                if(list.get(j) > list.get(j+1)) {
                    temp = list.get(j);
                    list.set(j, list.get(j+1));
                    list.set(j+1, temp);
                }
            }
        }
    }

    public void byGnome(List<Integer> list) {
        int i = 1;
        while(i < list.size()) {
            if(i == 0 || list.get(i - 1) <= list.get(i))
                i++;
            else {
                int temp = list.get(i);
                list.set(i, list.get(i - 1));
                list.set(i - 1, temp);
                i--;
            }
        }
    }
}
