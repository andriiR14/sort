package org.ariabchuk.sort;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class SortTest {

    @Test
    public void TestBubbleSort() {
        System.out.println("TestBubbleSort");
        List<Integer> unsortedList = Arrays.asList(2, 7, 15, 8, 11);
        System.out.println("unsorted list");
        System.out.println(Arrays.toString(unsortedList.toArray()));
        List<Integer> sortedList = Arrays.asList(2, 7, 8, 11, 15);
        new Sort().byBubble(unsortedList);
        for (int i = 0; i < sortedList.size(); i++) {
            Assert.assertTrue(sortedList.get(i) == unsortedList.get(i));
            System.out.println("Sorted list '" + i + "' element: '" + sortedList.get(i) + "'");
        }
    }

    @Test
    public void TestGnomeSort() {
        System.out.println("TestGnomeSort");
        List<Integer> unsortedList = Arrays.asList(2, 7, 15, 8, 11);
        List<Integer> sortedList = Arrays.asList(2, 7, 8, 11, 15);
        System.out.println("unsorted list");
        System.out.println(Arrays.toString(unsortedList.toArray()));
        new Sort().byGnome(unsortedList);
        for (int i = 0; i < sortedList.size(); i++) {
            Assert.assertTrue(sortedList.get(i) == unsortedList.get(i));
            System.out.println("Sorted list '" + i + "' element: '" + sortedList.get(i) + "'");
        }
    }

}